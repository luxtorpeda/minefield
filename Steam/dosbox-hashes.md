## Windows Vanilla

| SHA1 | Version |
|---	|---	|
| c30c133f95547c6d91907c17073945ed93a580e4 | v0.74 |
| 2470732961a35fd9a8708ef463cc999cdbcd32f2 | v0.74 |
| 537a12c14d6937d2d3039b8639e812351fb4f505 | v0.72 |
| 95b0c49b0bc67de3ed6f7fa9d7db9bcd67412764 | v0.71 |
| af0d70f97dea0aba623151779f6d6a96c2a49b6b | v0.63 |

## Windows Forks

| SHA1 | Version |
|---	|---	|
| 7b5af091bfae611f52db90e6d31622fd88192e5d | Daum (SVN: JAN 25, 2015) |
| e56a262a6008bcdb38a583b14d74528d332adae2 | Daum (SVN: JAN 25, 2015) (x64) |
| 9fb00f467890937dff31b67bf662e2462f961fc5 | Daum (SVN: JAN 27, 2014) |
| fc50443ac219a5ca2f452850a8b42cc97f7b0597 | Daum (SVN: 03-20-2011) |
| d40aa9f8c4c3fec370bf62cb7824c1cc21ea97cc | SVN-lfn (February 26, 2017) |

## Linux Vanilla

| SHA1 | Version |
|---	|---	|
| 6731a6ad9bab67c1b0ba23ed68a77b03c04ea7ee | v0.74 |
| cd9356c320ccca750b9b5d4cdc5312f71549f572 | v0.74 |
| ed33e690a0bd985c425dc3192e75eb3f92b15d89 | v0.74 |

## Linux Forks

| SHA1 | Version |
|---	|---	|
| e9ce257c8602b8569b0d375664c4cc2de7003720 | Daum (SVN: JAN 27, 2014) |
