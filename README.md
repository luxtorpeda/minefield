# Minefield

Repository for sharing configuration files and snippets for various investigations

## Needs Autoconfig

| ID | Name | Status
|---	|---	|---	
| 329650 | NAM | Pending |
| 358380 | Wacky Wheels | Pending |
| 358420 | Xenophage: Alien Bloodsport | Pending |